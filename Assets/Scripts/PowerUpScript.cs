﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpScript : MonoBehaviour {

	public enum PowerUpStatus{
		SPEEDUP, SLOWDOWNAGE
	}

	public PowerUpStatus _powerUpStatus;

	public float _powerUpSpeed;
	public float _powerUpTime;
	public float _reduceAge;
	public AudioClip eatenSound;

	private GameObject original;

	// Use this for initialization
	void Start () {

	}

	void SetOriginal(GameObject orig) {
		original = orig;
	}

	void OnTriggerEnter(Collider other)
	{
		var alienComponent = other.GetComponent<Alien>();
		var ageState = alienComponent
			? alienComponent.GetAgeState()
			: Alien.AgeState.NONE;

		if(ageState == Alien.AgeState.BABY
				|| ageState == Alien.AgeState.TEEN)
		{
			switch (_powerUpStatus)
			{
				case PowerUpStatus.SPEEDUP:
					SpeedUp(other);
					break;
				case PowerUpStatus.SLOWDOWNAGE:
					SlowDownAgeTime(other);
					break;
			}
		}
	}

	// Update is called once per frame
	void Update () {
	}

	private void SlowDownAgeTime(Collider player){
		var alienComponent = player.GetComponent<Alien>();
		if (alienComponent != null)
		{
			alienComponent.ReduceAge(_reduceAge);
		}

		DeactivateGameObject();
	}

	/// <summary>
	/// Speeds up the player or alien when a power up is touched.
	/// </summary>
	/// <param name="alien">Player.</param>
	private void SpeedUp(Collider alien){
		var playerComponent = alien.GetComponent<PlayerScript>();

		if (playerComponent != null)
		{
			playerComponent._playerMovementSpeed = _powerUpSpeed;
		}

		var alienComponent = alien.GetComponent<Alien>();

		if (alienComponent != null)
		{
			alienComponent.SetTempSpeed(_powerUpSpeed, _powerUpTime);
		}

		DeactivateGameObject();
	}

	/// <summary>
	/// Deactivates the game object.
	/// </summary>
	private void DeactivateGameObject(){
		gameObject.SetActive(false);
		PlaySound(eatenSound);
		var world = transform.parent.GetComponent<World>();
		if (world)
			world.AddFruitToRespawn(original);
		Destroy(gameObject);
	}

	void PlaySound (AudioClip sound) {
		var asp = GameObject.FindObjectOfType<AudioSourcePool>();
		if (asp && sound)
			asp.PlayAtPoint(sound, transform.position);
	}
}
