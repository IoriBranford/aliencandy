﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {
	private List<GameObject> fruitsToRespawn;
	// Use this for initialization
	void Start () {
		fruitsToRespawn = new List<GameObject>();
		var originalFruits = GameObject.FindGameObjectsWithTag("Pickup");
		foreach (var fruit in originalFruits) {
			fruit.SetActive(false);
			SpawnFruit(fruit);
		}
	}

	void Update () {
	}

	void SpawnFruit(GameObject fruit) {
		var newFruit = Instantiate(fruit, transform);
		newFruit.SetActive(true);
		newFruit.BroadcastMessage("SetOriginal", fruit);
	}

	public void AddFruitToRespawn(GameObject fruit) {
		fruitsToRespawn.Add(fruit);
	}

	public GameObject FindNearestWithTag(string tag, Vector3 point) {
		var objects = GameObject.FindGameObjectsWithTag(tag);
		GameObject nearestObject = null;
		float nearestObjectDistSq = 10000000000;
		foreach (var obj in objects) {
			var pointToObject = obj.transform.position - point;
			var distSq = pointToObject.sqrMagnitude;
			if (distSq < nearestObjectDistSq) {
				nearestObject = obj;
				nearestObjectDistSq = distSq;
			}
		}
		return nearestObject;
	}

	public GameObject FindNearestYoung (Vector3 point) {
		var objects = GameObject.FindGameObjectsWithTag("Character");
		GameObject nearestYoung = null;
		float nearestYoungDistSq = 10000000000;
		foreach (var obj in objects) {
			var alienComp = obj.GetComponent<Alien>();
			if (!alienComp
			|| alienComp.GetAgeState() == Alien.AgeState.EGG
			|| alienComp.GetAgeState() == Alien.AgeState.ADULT)
				continue;

			var pointToYoung = obj.transform.position - point;
			var distSq = pointToYoung.sqrMagnitude;
			if (distSq < nearestYoungDistSq) {
				nearestYoung = obj;
				nearestYoungDistSq = distSq;
			}
		}
		return nearestYoung;
	}

	public void OnNewDayState(DayNight.DayState dayState) {
		switch (dayState) {
			case DayNight.DayState.DAWN:
				foreach (var fruit in fruitsToRespawn) {
					SpawnFruit(fruit);
				}
				fruitsToRespawn.Clear();
				break;
		}
	}
}
