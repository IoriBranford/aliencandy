﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
	private static bool created = false;

	void Awake() {
		if (!created) {
			DontDestroyOnLoad(gameObject);
			created = true;
		}
	}

	public void LoadScene(string sceneName) {
		SceneManager.LoadScene(sceneName);
	}
}
