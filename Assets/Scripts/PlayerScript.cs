﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    public float _playerMovementSpeed;
    public GameObject _mainCameraComponent;
    public AudioClip _callSound;

    private bool _triggerButtonClicked = false;
    private Vector3 _setTrapLocation;

    private GameObject _grabbedAlien;
    private bool _pauseGame;
    private float _originalTimeScale;

	// Use this for initialization
	void Start () {
        StartCoroutine(PauseGame());
	}
	
	// Update is called once per frame
	void Update () {

        // Movement
        var horizontal = Input.GetAxis("Horizontal")*Time.deltaTime*_playerMovementSpeed;
        var vertical = Input.GetAxis("Vertical") * Time.deltaTime * _playerMovementSpeed;
        Vector3 input = new Vector3(horizontal, 0, vertical);
        transform.position += input;

        if(EatFeedButton()){
            //TODO : Implement Eat Fead function
            Debug.Log("EatFeedButton");
        }

        if(CallbackButton()){
            //TODO Implement Callback function
            Debug.Log("Callback Button");
            CallAllBabies();
        }

        if(TrapButton()){
            // TODO Implement Grab functionality
            Debug.Log("Trap button");

            var nearestBaby = GetNearestAlien();

            if(nearestBaby != null){
                nearestBaby.SetTrap();
            }
        }

        if(_pauseGame){
            Time.timeScale = 0;
        }
    }

    // Used to pause the game when return key is
    IEnumerator PauseGame()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                // TODO: make pause menu appear here
                if (Time.timeScale == 0)
                {
                    Time.timeScale = 1;
                }
                else
                {
                    Time.timeScale = 0;
                }
            }
            yield return null;
        }
    }

    public Vector3 GetTrapLocation(){
        return _setTrapLocation;
    }

    private Alien GetNearestAlien(){
        Debug.Log("Got nearest alien");
        var currPosition = transform.position;
        var aliens = transform.parent.GetComponentsInChildren<Alien>();
        var nearestAlien = aliens.Where(alien =>
                                        (alien.GetAgeState() == Alien.AgeState.BABY || 
                                         alien.GetAgeState() == Alien.AgeState.TEEN)
                                       && Vector3.Distance(alien.transform.position, transform.position) < 3.0f
                                       && alien != gameObject.GetComponent<Alien>())
                                 .OrderBy(alien => Vector3.Distance(transform.position, alien.transform.position))
                                 .FirstOrDefault();

        return nearestAlien != null ? nearestAlien : null;
    }


    private bool EatFeedButton(){
        var rightClick = Input.GetKeyDown(KeyCode.Mouse1);
        var aButton = Input.GetKeyDown(KeyCode.JoystickButton0);

        return rightClick || aButton;
    }

    private bool TrapButton(){
        var bButton = Input.GetKeyDown(KeyCode.JoystickButton1);
        var leftShift = Input.GetKeyDown(KeyCode.LeftShift);

        _setTrapLocation = transform.position;

        return bButton || leftShift;
    }

    private bool CallbackButton(){
        var yButton = Input.GetKey(KeyCode.JoystickButton3);
        var spaceBar = Input.GetKey(KeyCode.Space);

        return yButton || spaceBar;
    }

    private void CallAllBabies(){
        var world = transform.parent;
        world.BroadcastMessage("CallToPlayerLocation", transform.position);
	PlaySound(_callSound);
    }

    private bool GrabButton(){
        bool triggerButton = TriggerButtonOnHold();
        bool leftClick = Input.GetMouseButton(0);

        return triggerButton || leftClick;
    }

    private bool TriggerButtonOnHold(){
        if( Input.GetAxisRaw("Grab") != 0.0f){
            if(_triggerButtonClicked == false){
                _triggerButtonClicked = true;
            }
        }

        if(Input.GetAxisRaw("Grab") == 0){
            _triggerButtonClicked = false;
        }

        return _triggerButtonClicked;
    }

	void PlaySound (AudioClip sound) {
		var asp = GameObject.FindObjectOfType<AudioSourcePool>();
		if (asp && sound)
			asp.PlayAtPoint(sound, transform.position);
	}
}
