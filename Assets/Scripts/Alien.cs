﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : MonoBehaviour {
	public enum AgeState {
		NONE = -1, EGG, BABY, TEEN, ADULT
	};

	public float _startAge;
	public float _teenAge;
	public float _adultAge;
	public float _deathAge;
	public float _babyMoveSpeed;
	public float _teenMoveSpeed;
	public float _adultMoveSpeed;
	public float _babyEatTime;
	public float _teenEatTime;
	public float _adultBumpStunTime;
	public GameObject _creatableObject;
	public AudioClip _eatBabySound;
	public AudioClip _eatenSound;
	public AudioClip _trappedSound;

    private bool _isCalled;
    private Vector3 _calledLocation;

	private float age;
	private float moveSpeed;
	private float speedResetTimer;

	public AgeState GetAgeState () {
		if (age < 0)
			return AgeState.EGG;
		if (age < _teenAge)
			return AgeState.BABY;
		if (age < _adultAge)
			return AgeState.TEEN;
		return AgeState.ADULT;
	}

	// Use this for initialization
	void Start () {
		age = _startAge;
		ResetMoveSpeed();
	}

	void PlaySound (AudioClip sound) {
		var asp = GameObject.FindObjectOfType<AudioSourcePool>();
		if (asp && sound)
			asp.PlayAtPoint(sound, transform.position);
	}

	void ResetMoveSpeed () {
		speedResetTimer = 0;
		switch (GetAgeState()) {
			case AgeState.EGG:
				moveSpeed = 0;
				break;
			case AgeState.BABY:
				moveSpeed = _babyMoveSpeed;
				break;
			case AgeState.TEEN:
				moveSpeed = _teenMoveSpeed;
				break;
			case AgeState.ADULT:
				moveSpeed = _adultMoveSpeed;
				break;
		}
	}

    public float GetMoveSpeed(){
        return moveSpeed;
    }

    public void CallToPlayerLocation(Vector3 calledPosition){
        _isCalled = true;
        _calledLocation = calledPosition;
    }

    private bool IsChildOrTeen(){
        var isChild = GetAgeState() == AgeState.BABY;
        var isTeen = GetAgeState() == AgeState.TEEN;

        return isChild || isTeen;
    }

    private void Think(){
	    var world = GetComponentInParent<World>();
	    var dayTimeStatus = world.GetComponentInChildren<DayNight>();
	    var sleep = false;
	    if(dayTimeStatus != null){
		    var dayState = dayTimeStatus._dayState;

		    if(dayState == DayNight.DayState.NIGHT && GetAgeState() == AgeState.ADULT){
			    sleep = true;
		    }
	    }

        var position = transform.position;
        var targetPosition = position;
        if (_isCalled && IsChildOrTeen())
        {
            targetPosition = _calledLocation;
        }
        else if (!sleep)
        {
            var targetObject = GetAgeState() != AgeState.ADULT
                ? world.FindNearestWithTag("Pickup", position)
                : world.FindNearestYoung(position);
            if (targetObject)
            {
                targetPosition = targetObject.transform.position;
            }
        }

        var rigidbody = GetComponent<Rigidbody>();
        if (rigidbody)
        {
            var targetVector = targetPosition - position;

            if (_isCalled)
            {
                _isCalled &= targetVector.sqrMagnitude >= 0.25f;
            }
            var velocity = targetVector.normalized * moveSpeed;
            var accel = velocity - rigidbody.velocity;
            rigidbody.AddForce(rigidbody.mass * accel * 4);

            velocity = rigidbody.velocity;
            if (velocity.sqrMagnitude > 0)
            {
                float angle = Mathf.Atan2(velocity.x, velocity.z);
                transform.rotation = Quaternion.AxisAngle(Vector3.up, angle);
            }
        }
    }

	void CreateNewObject() {
		if (!_creatableObject)
			return;
		var worldTransform = transform.parent;
		Instantiate(_creatableObject, transform.position, Quaternion.identity, worldTransform);
	}

    public void SetTrap(){
        if (GetAgeState() == AgeState.BABY
            || GetAgeState() == AgeState.TEEN)
        {
            Debug.Log("set trap...");
            CreateNewObject();
        }
    }

	// Update is called once per frame
	void Update () {
		float dt = Time.deltaTime;
		var oldAgeState = GetAgeState();
		age = age + dt;
		if (age >= _deathAge) {
			Destroy(gameObject);
			return;
		}
		var newAgeState = GetAgeState();

		var gauge = transform.Find("AlienGauge");

		if (oldAgeState != newAgeState) {
			ResetMoveSpeed();
			switch (newAgeState) {
				case AgeState.BABY:
					CreateNewObject();
					Destroy(gameObject);
					return;
				case AgeState.ADULT:
					Destroy(gauge.gameObject);
					gauge = null;
					break;
			}
		}

		//if (newAgeState == AgeState.BABY
		//|| newAgeState == AgeState.TEEN) {
		//	if (Input.GetKeyDown(KeyCode.Space)) {
		//		CreateNewObject();
		//	}
		//}

		speedResetTimer = speedResetTimer - dt;
		if (speedResetTimer <= 0) {
			ResetMoveSpeed();
		}

		if (gauge) {
			var renderer = gauge.GetComponent<Renderer>();
			var material = renderer.material;
			material.SetFloat("_Fill", age / _adultAge);
			material.SetFloat("_Delta", dt / _adultAge);
		}
	}

	void FixedUpdate () {
		var player = GetComponent<PlayerScript>();
		if (!player) {
			Think();
		}
	}

	public void SetTempSpeed (float tempSpeed, float time) {
		moveSpeed = tempSpeed;
		speedResetTimer = time;
	}

	public void ReduceAge (float decAge) {
		age = Mathf.Max(0, age - decAge);
	}

	void OnCollisionEnter(Collision collision) {
		foreach (var contact in collision.contacts) {
			var otherCollider = contact.otherCollider;
			var otherAlien = otherCollider.GetComponent<Alien>();
			var otherAgeState = otherAlien
				? otherAlien.GetAgeState()
				: AgeState.NONE;

			switch (GetAgeState()) {
				case AgeState.BABY:
					switch (otherAgeState) {
						case AgeState.ADULT:
							PlaySound(_eatenSound);
							PlaySound(_eatBabySound);
							Destroy(gameObject);
							otherAlien.SetTempSpeed(0, _babyEatTime);
							break;
					} break;
				case AgeState.TEEN:
					switch (otherAgeState) {
						case AgeState.ADULT:
							PlaySound(_eatenSound);
							PlaySound(_eatBabySound);
							Destroy(gameObject);
							otherAlien.SetTempSpeed(0, _teenEatTime);
							break;
					} break;
				case AgeState.ADULT:
					switch (otherAgeState) {
						case AgeState.ADULT:
							PlaySound(_trappedSound);
							SetTempSpeed(0, _adultBumpStunTime);
							break;
					} break;
			}
		}
	}

	void OnTriggerEnter(Collider otherCollider)
	{
		var trap = otherCollider.GetComponent<TrapScript>();
		switch (GetAgeState()) {
			case AgeState.TEEN:
			case AgeState.ADULT:
				if (trap) {
					SetTempSpeed(0, trap._trapDestructionDelayTime);
					PlaySound(_trappedSound);
				} break;
		}
	}

	void OnNewDayState(DayNight.DayState dayState) {
		switch (GetAgeState()) {
			case AgeState.BABY:
			case AgeState.TEEN:
				// TODO: return to ship
				break;

			case AgeState.ADULT:
				switch (dayState) {
					case DayNight.DayState.DAWN:
						CreateNewObject();
						break;
				} break;
		}
	}
}
