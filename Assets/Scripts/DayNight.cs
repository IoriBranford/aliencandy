﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour {
	public float _secondsPerDay;

	public enum DayState{
		DAWN, DAY, DUSK, NIGHT
	}

	public DayState _dayState {
		get {
			float zRotationRadians = Mathf.Deg2Rad
				* gameObject.transform.rotation.eulerAngles.z;
			float sin = Mathf.Sin(zRotationRadians);
			float cos = Mathf.Cos(zRotationRadians);

			if (sin >= 0)
			{
				if (cos > .5)
					return DayState.DAWN;
				return DayState.DAY;
			}
			if (cos < -.5)
				return DayState.DUSK;
			return DayState.NIGHT;
		}
	}

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		var oldDayState = _dayState;

		float angle = 360 * Time.deltaTime / _secondsPerDay;
		gameObject.transform.Rotate(Vector3.forward, angle);

		var newDayState = _dayState;
		if (oldDayState != newDayState) {
			var world = transform.parent;
			world.BroadcastMessage("OnNewDayState", newDayState);
		}

		float zRotationRadians = Mathf.Deg2Rad*gameObject.transform.rotation.eulerAngles.z;
		float sin = Mathf.Sin(zRotationRadians);
		float cos = Mathf.Cos(zRotationRadians);

		//Debug.Log(string.Format("Angle {0} zRotationRadians {1} sin {2} cos {3} Day State {4}", 
					//angle, zRotationRadians, sin, cos, _dayState));
	}
}
