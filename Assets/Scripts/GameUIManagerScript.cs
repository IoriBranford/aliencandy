﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameUIManagerScript : MonoBehaviour {
    
    public Text _babyCounterText;
    public Text _teenCounterText;

    private int _babyCount = 0;
    private int _teenCount = 0;

	// Use this for initialization
	void Start () {
        SetText();
	}
	
	// Update is called once per frame
	void Update () {
        var world = transform.parent;
        var listOfAliens = world.GetComponentsInChildren<Alien>();

        _babyCount = listOfAliens
                        .Where(alien => alien.GetAgeState() == Alien.AgeState.BABY)
                        .ToList().Count;
        
        _teenCount = listOfAliens
            .Where(alien => alien.GetAgeState() == Alien.AgeState.TEEN)
            .ToList().Count;

        SetText();
	}

    private void SetText(){
        if(_babyCounterText != null){
            _babyCounterText.text = string.Format("Baby: {0}", _babyCount);
        }

        if(_teenCounterText != null){
            _teenCounterText.text = string.Format("Teen: {0}", _teenCount);
        }
    }
}
