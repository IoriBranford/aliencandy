﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour {
    private GameObject startScene, mainMenu;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown)
        {
            SetShowing(false);
        }
    }
    
    public void SetShowing (bool show)
    {
        if (!startScene)
            startScene = GameObject.Find("StartScene");
        startScene.SetActive(!show);
        if (!mainMenu)
            mainMenu = GameObject.Find("MainMenu");
        mainMenu.SetActive(!show);

        gameObject.SetActive(show);
    }
}
