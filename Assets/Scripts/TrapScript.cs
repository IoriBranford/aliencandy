﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapScript : MonoBehaviour {

	/// <summary>
	/// The trap destruction delay time. Based by seconds
	/// </summary>
	public int _trapDestructionDelayTime;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	private void OnTriggerEnter(Collider byStander)
	{
		var enemyScript = byStander.gameObject.GetComponent<Alien>();
		if (enemyScript) {
			switch (enemyScript.GetAgeState()) {
				case Alien.AgeState.ADULT:
					StartCoroutine(DelayTrapDestruction(byStander));
					Debug.Log("Adult stunned...");
					break;
				case Alien.AgeState.TEEN:
					// TODO: implement logic to trap teen when almost an adult
					Debug.Log("Teen trapped...");
					break;
			}
		}
	}

	IEnumerator DelayTrapDestruction(Collider byStander){
		var enemyScript = byStander.gameObject.GetComponent<Alien>();
		enemyScript.SetTempSpeed(0, _trapDestructionDelayTime);
		yield return new WaitForSeconds(_trapDestructionDelayTime);
		DeactivateGameObject();
	}

	/// <summary>
	/// Deactivates the game object.
	/// </summary>
	private void DeactivateGameObject()
	{
		gameObject.SetActive(false);
		Destroy(gameObject);
	}
}
